/* Exercice basique : sum
    À peine plus compliqué, mais cette fois c’est à vous de jouer:
    Réimplémentez la fonction
    sum, qui retourne la somme des valeurs d’un tableau, de manière récursive.
    Gérez d’abord le cas terminal, puis l’appel récursif. */

    const sum = (a) => {
        if (a[0] === undefined)
            return 0
        return a[0] + sum(a.slice(1))
    }

/* Exercice basique: map
    Réimplementez la fonction map sous la forme map(a,f) qui renvoie une copie
    du tableau a, dont les éléments ont été transformés en appelant f. */

/* Explications du code: 
map prend 2 arguments: a = tableau et f une fonction
on établi le cas de sortie: si l'index 0 du tableau === undefined (= est vide) alors on renvoie un tableau vide: 
ça va le concatener avec le reste du tableau sans changer son contenu (opération neutre)
puis on établi le cas classique: on applique la fonction f sur le 1er élément du tableau a,
 puis on le concatene au resultat suivant(en ayant pris soin de lui passer la liste a raccourcie (le a.slice(1)
renvoie un tableau ou on a enlevé le 1er élément du tableau)) */

    const map = (a,f) => {
        if (a[0] === undefined)
            return []
        return [f(a[0])].concat(map(a.slice(1), f))
    }

    /* pour tester :

    let f = x => x*x
    const a = [1,2,3]
    map(a,f) devra renvoyer un nouveau tableau [1,4,9] /*
    
/* Exercice intermédiaire: first
    À vous de jouer: spécifiez avec des tests unitaires puis implémentez la fonction
    first(a, f) qui renvoie le premier élément e de a pour lequel f(e)
    renvoie true.
    Pensez à spécifier les cas à problème:

    tableau vide
    pas d’élément trouvé vérifiant f */

/*  Variante
    Écrivez à partir de first la fonction some(a, f) qui renvoie true si un
    des élément de a vérifie f. */