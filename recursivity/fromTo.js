/* Exercices sur map/filter/reduce
    fromTo
    Réalisez la fonction fromTo, qui génère un tableau comportant tous les
    entiers entre le premier argument fourni et le dernier.
    fromTo(2,5) === [2,3,4,5]

    Pour vous aider à démarrer:
    let a = Array(5) // tableau vide de longueur 5
    Array.from(a) // tableau de 5 cases avec la valeur undefined */

    let fromTo = (x,y) => {
        if (y === undefined) return [x].concat()// s'il n'y a qu'une seule donnée, on retourne un tableau avec seulement cette donnée
        a = Array.from(Array(y-x+1))  // on construit un tableau avec le nb d'elements voulus et on rempli chaque case d'undefined
        return a.map((_,index) => a[index] = x + index) // on remplit chaque case
    }

    // Fonction produisant les nombres pairs non divisible par trois et < à 100
    fromTo(1,99).filter( n => n % 2 === 0 ).filter( n => n % 3 !== 0)

    // Fonction produisant les nombres inférieurs à 100 vérifiant une fonction f 
    fromTo(1,99).filter( n => n % 2 === 0 ).filter( n => n % 3 !== 0).reduce(x,y) => x + y
