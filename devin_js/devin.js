/*
1er code sans bouton html

console.log('Bienvenue dans le jeu du Devin!\nL\'ordinateur a choisi un chiffre, à toi de le deviner!')
let goal = Math.floor(Math.random() * 50)
console.log(`Pour tester le code facilement, voici le nombre à deviner: ${goal}`)

let proposal = goal + 1

function guess(proposal, goal){
    if (proposal == goal) return (`Bravo!`)
    return (proposal < goal ? 'Trop petit!' : 'Trop grand!')
}

while (proposal != goal) {
    proposal = prompt('Entre un nombre entre 0 et 50:') 
    console.log(guess(proposal, goal))
}
*/


/* 2eme code, avec déclenchement par bouton html */

function guess(proposal, goal){
    if (proposal == goal) return (`Bravo!`)
    return (proposal < goal ? 'Trop petit!' : 'Trop grand!')
}

function devin(){
    console.log('Bienvenue dans le jeu du Devin!\nL\'ordinateur a choisi un chiffre, à toi de le deviner!')
    const goal = Math.floor(Math.random() * 50)
    console.log(`Pour tester le code facilement, voici le nombre à deviner: ${goal}`)
    let proposal
    while (proposal != goal) {
        proposal = prompt('Entre un nombre entre 0 et 50:') 
        console.log(guess(proposal, goal))
    }
}

/* On aurait pu initialiser found = false pour entrer dans la boucle et une fois dedans faire found = guess == target 
soit si le guess du user est = au chiffre à deviner alors ça renvoie true qu'on stocke ds la variable found,
donc on sort de la boucle. */ 