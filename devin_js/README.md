### Bienvenue dans le jeu du devin ##
#### Avoir ouvert la console avant de lancer la page html afin de voir les indices ####
##### Pseudo code: #####
Accueillir le joueur<br/>
Stocker le chiffre choisi aléatoirement par l'ordinateur dans la variable 'goal'<br/>
Initialiser la variable 'proposal' à 'goal +1' afin d'entrer dans la boucle<br/>
Tant que proposal != goal<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demander à l'utilisateur d'entrer une proposal<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comparer proposal et goal<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Donner un indice à l'utilisateur<br/>
Féliciter le joueur

