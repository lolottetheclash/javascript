function fizzbuzz(i) {
    if (!(i % 15)) return 'fizzbuzz'
    if (!(i % 5)) return 'buzz'
    if (!(i % 3)) return 'fizz'
    return i
}
for (let i = 1; i<=100; i++) console.log(fizzbuzz(i)) 
