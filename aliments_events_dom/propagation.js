/*
Codez la fonctionnalité: click réduit l’affichage des nutriments
un click sur la liste des nutriments d’un plat ou sur le titre d’un plat va
rendre la liste des nutriments invisible, et uniquement celle là.

Rappel de la méthode:

  Déterminez l’événement et le tag html visé. Testez le déclenchement avec un
    console.log(…) dans le navigateur sur un élément exemple.
  Rajouter le comportement visé dans votre test.
  Codez la version définitive pour l’élément exemple.
  Généralisez à tous les éléments visés. Testez. (map …)
  Basculez le code dans un fichier, testez.

Ici:

  événement: click, tags html concernés:  dl dans un li, et le li englobant. 
  Prenez un des li, récupérer une représentation, et faites les essais dessus.
  comportement visé: élévemt visé qui devient caché.
  le click sur la dl ou sur le li  de l’exemple doit rendre la dl invisible.
  le click sur une dl~/un ~li quelconque doit rendre invisible la dl.
  Recharchez la page, tout doit marcher !
  */
 
  // on selectionne le 1er plat
let li = document.querySelector('li')

// on crée une fonction qui nous affiche l'évenement pour tester
function clicktest(e){
    console.log(e)
}
// on ajoute un event listener sur le 1er plat
li.addEventListener('click', clicktest)

// on selectionne la liste des nutriments du 1er plat, on passe l'attribut hidden à true pour les cacher
li.querySelector('dl').hidden = true

// on fait une fonction qui fait le truc précédent
function hideNutriment(e){
    this.querySelector('dl').hidden = true
}

// on applique le this de la fction hideNutriments à li
hideNutriment.call('li')

// on applique le addeventlistener à tous les li
document.querySelectorAll('li.plat').forEach(li => li.addEventListener('click', hideNutriment))


/*
Délégation d’événement
Dans l’exercice précédent, nous avons ajouté un événement sur chaque dl présent dans notre liste.
Utilisons plutôt la propagation d’événement, en réalisant la chose suivante:
Nous allons positionner un événement click et son callback associé sur l’élément ul englobant.
Si je clique sur un des éléments li, qu’est-ce que le callback positionné sur le ul peut faire ?
Quelle est la phase de propagation concernée ?
Testez en console
Pour vous convaincre que vous avez saisi le fonctionnement, réalisez un callback associé à l’événement 
click sur le tag ul qui affiche dans la console:
l’élément cible de l’événement
le nom de l’aliment sur lequel le clic se produit.
Hint: Event.target
Hint: Element.closest()
Un seul callback: click réduit l’affichage des nutriments
Positionnez un seul callback sur le ul qui réalise la même chose que précédemment: 
un click sur la liste des nutriments d’un plat va rendre la listeinvisible, et uniquement celle là.
  
on veut que quand on clique sur n'importe quel element contenu dans ul cela cache le <dl> correspondant
le eventtarget sera donc un li, dl, dt,p ,  ou dd
on met le eventlistener click sur ul
quelquesoit le target, il est inclus dans li, donc il faut trouver le li le plus proche pour cacher le dl
*/

function hideNutriment(ev) {
    ev.target.closest('li').querySelector('dl').hidden = true
} 

document.querySelector('ul').addEventListener('click', hideNutriment)
