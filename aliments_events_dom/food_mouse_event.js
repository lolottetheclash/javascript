// loaded with defer: DOM is ready
let plats = Array.from(document.querySelectorAll('.plat'))

const glucides = plat => parseFloat(
  Array.from(plat.querySelectorAll('dt'))  // find all dt's in plat
    .find(el => el.innerHTML.toLowerCase() === 'glucides') // keep the one with glucides
    .nextElementSibling // get to the dd
    .innerHTML)

let ketodishes = plats.filter(plat => glucides(plat) < 10)

const nutriment = (plat, nutriment) => parseFloat(
  Array.from(plat.querySelectorAll('dt'))  // find all dt's in plat
    .find(el => el.innerHTML.toLowerCase() === nutriment) // keep the one with the nutriment
    .nextElementSibling // get to the dd
    .innerHTML) 

const lipides = plat => nutriment(plat, 'lipides')
const calories = plat => nutriment(plat, 'calories')
const proteines = plat => nutriment(plat, 'protéines')

const plat2js = (plat) => 
  ({
    name: plat.querySelector('p').innerHTML,
    weight: parseFloat(plat.attributes['data-weight'].value),
    caloriesTotal: parseFloat(plat.attributes['data-calories'].value),
    calories: calories(plat),
    lipides: lipides(plat),
    glucides: glucides(plat),
    proteines: proteines(plat)
  })



const platjs2tr = p =>
  `<tr>
     <td>${p.name}</td><td>${p.calories}</td><td>${p.glucides}</td>
   </tr>
`

const table = document.createElement('table')
table.innerHTML = plats.map(p => platjs2tr(plat2js(p))).join("\n")
document.body.appendChild(table)

////////////////////////////////////////// Programmez le comportement suivant //////////////////////////////////////////////////
/* 
A partir de la vue liste, quand le curseur de la souris est sur un élément de la liste, l’affichage change.
Quand le curseur quitte l’élément, l’affichage initial est rétabli.

Piste: regardez la description des événements mouseenter, mouseover et mouseleave sur le MDN.

Méthode:
Trouvez l’événement utilisateur qui va servir de déclencheur: tester avec un callback à base de console.log(), 
sur un élément de la liste
Associez une classe CSS à l’affichage voulue: testez à partir de l’interface de développement de votre navigateur
quand l’événement est déclenché, rajoutez la classe !
Maintenant, appliquez le code précédent à chaque élément de la liste
*/


// On récupère tous les titres de la liste
titres = Array.from(document.querySelectorAll('p'))

// Fonction qui ajoute la classe qui donne la couleur rouge
function enter(){
  this.classList.add("toRed")
}

// Fonction qui enlève la classe qui donnait la couleur rouge
function out(){
  this.classList.remove("toRed")
}

//cPour chaque titre de la liste, on lui ajoute les comportements liés au évenements de la souris
titres.forEach(titre =>{
  titre.addEventListener('mouseenter', enter)
  titre.addEventListener('mouseleave', out)
  }  
)

// Pour chaque titre, quand la souris clique dessus, on lui ajoute/enlève la classe display none qui s'applique sur le next sibling
titres.forEach(titre => titre.addEventListener('click',function(event) {
  this.nextElementSibling.classList.toggle("noList");
}))


////////////////////////////////////////// Programmez le comportement suivant //////////////////////////////////////////////////
//  Libre


function ratadd() {
  // création de l'élément à inserer
  var image = document.createElement("img")
  image.src ="rata.jpg"
  image.setAttribute("class", "photo");
  //ajout au bon endroit
  plats[1].appendChild(image)
}



document.addEventListener('keyup', ratadd);

