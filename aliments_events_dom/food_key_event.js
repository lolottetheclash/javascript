
const nutriment = (plat, nutriment) => parseFloat(
  Array.from(plat.querySelectorAll('dt'))  // find all dt's in plat
    .find(el => el.innerHTML.toLowerCase() === nutriment) // keep the one with the nutriment
    .nextElementSibling // get to the dd
    .innerHTML) 

const glucides = plat => nutriment(plat, 'glucides')
const lipides = plat => nutriment(plat, 'lipides')
const calories = plat => nutriment(plat, 'calories')
const proteines = plat => nutriment(plat, 'protéines')

const platEl2js = (plat) => ({
  name: plat.querySelector('p').innerHTML,
  weight: parseFloat(plat.attributes['data-weight'].value),
  caloriesTotal: parseFloat(plat.attributes['data-calories'].value),
  calories: calories(plat),
  lipides: lipides(plat),
  glucides: glucides(plat),
  proteines: proteines(plat)
})

let plats = Array.from(document.querySelectorAll('.plat'))
plats.map(platEl2js) // je passe à plats.map une valeur fonction, que je vais appliquer à chaque élément de plat
const aliments = plats.map(platEl2js)
console.log(aliments)

/* Génération dynamique de html: lorsque la touche "t" est appuyée :
Rajoutez dans la page la liste des aliments, mais cette fois sous la forme d’un tableau html . */


const plats = Array.from(document.querySelectorAll('.plat')).map(platEl2js)

const plat2tr = p =>
      `<tr>
     <td>${p.name}</td><td>${p.calories}</td><td>${p.glucides}</td>
   </tr>
`
const plats2table = (plats) => {
  const table = document.createElement('table')
  table.id="foodlist"
  table.innerHTML = plats.map(plat2tr).join('\n')
  return table
}

const redraw = el => {
  document.querySelector('#foodlist').replaceWith(el)
}

// key t: redraw with table view
document.addEventListener('keyup', e => {
  if (e.key === 't')
    redraw(plats2table(plats))
})

/*
--------------------------------------------------------------------------------------------------------------------------------

En vous inspirant de la fonctionnalité implémentée, codez le comportement suivant: «à l’appui sur la touche l, 
l’affichage passe en mode liste».

<ul>
  <li class="plat" data-weight="100" data-calories="341">
    <p>Paris brest</p>
    <dl>
      <dt>calories</dt>
      <dd>341</dd>
      <dt>lipides</dt>
      <dd>8</dd>
      <dt>glucides</dt>
      <dd>33</dd>
      <dt>protéines</dt>
      <dd>17</dd>
    </dl>
  </li>
</ul> 
*/

// je crée un plat sous forme de liste
const platToList = p => 
`
  <li class="plat" data-weight=${p.weight} data-calories=${p.caloriesTotal}>
    <p>${p.name}</p>
    <dl>
      <dt>calories</dt>
      <dd>${p.calories}</dd>
      <dt>lipides</dt>
      <dd>${p.lipides}</dd>
      <dt>glucides</dt>
      <dd>${p.glucides}</dd>
      <dt>protéines</dt>
      <dd>${p.proteines}</dd>
    </dl>
  </li>
`

// Je transforme ma liste objets js en listes html
  const completeList = (plats) => {
    const liste = document.createElement('ul')
    liste.id = 'liste'
    liste.innerHTML = plats.map(platToList).join('')
    return liste 
}

// Creation de la fonction qui transforme la table existente (si on a appuyé sur la touche 't') en liste
const tableToList = (remplace) => {
  table = document.getElementById('foodlist')
  table.replaceWith(remplace)
}

// je crée la fonction qui détecte l'appui sur la touche i
const ikey = (e) => {
  if (e.key === 'l')
    tableToList(completeList(plats))
}

// j'applique l'évenement 'keyup' (appui sur une touche, déclenche la fonction ikey()) à tout le document
document.addEventListener('keyup', ikey)
