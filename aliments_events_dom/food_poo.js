/*  
NE PAS OUBLIER DE CHANGER LE NOM DU FICHIER JS LIE A ALIMENTS.HTML POUR TESTER

Refactorisation du code en utilisant les prototypes
on ajoute de la ligne 10 à la ligne 31
on ajoute new Food à la ligne 44
on change en ${(p.calories)} en  ${(p.calories())} à la ligne 59
*/

function Food(nameOrObject,weight,lipides,glucides,proteines){
    if (nameOrObject instanceof Object){
        Object.assign(this, nameOrObject)
    }
    else{
        this.name = nameOrObject
        this.weight = weight
        this.lipides = lipides
        this.glucides = glucides
        this.proteines = proteines
        this.calories = () => this.proteines * 4 + this.glucides * 4 + this.lipides * 9
        this.caloriesTotal = () => (this.calories * weight)/100
    }
}

Food.prototype.calories = function(){ 
    return this.proteines * 4 + this.glucides * 4 + this.lipides * 9
}

Food.prototype.caloriesTotal = function(){ 
    return (this.calories * weight)/100
}

const nutriment = (plat, nutriment) => parseFloat(
    Array.from(plat.querySelectorAll('dt'))  // find all dt's in plat
      .find(el => el.innerHTML.toLowerCase() === nutriment) // keep the one with the nutriment
      .nextElementSibling // get to the dd
      .innerHTML) 
  
  const glucides = plat => nutriment(plat, 'glucides')
  const lipides = plat => nutriment(plat, 'lipides')
  const calories = plat => nutriment(plat, 'calories')
  const proteines = plat => nutriment(plat, 'protéines')
  
  const platEl2js = (plat) => new Food({
    name: plat.querySelector('p').innerHTML,
    weight: parseFloat(plat.attributes['data-weight'].value),
    //caloriesTotal: parseFloat(plat.attributes['data-calories'].value),
    //calories: calories(plat),
    lipides: lipides(plat),
    glucides: glucides(plat),
    proteines: proteines(plat)
  })
  
  const plats = Array.from(document.querySelectorAll('.plat')).map(platEl2js)

  // toFixed(2) : Converti un nombre en string en gardant 2 décimales
  const plat2tr = p =>
        `<tr>
       <td>${p.name}</td><td>${(p.calories()).toFixed(2)}</td><td>${p.glucides}</td>
     </tr>
  `
  const plats2table = (plats) => {
    const table = document.createElement('table')
    table.id="foodlist"
    table.innerHTML = plats.map(plat2tr).join('\n')
    return table
  }
  
  const redraw = el => {
    document.querySelector('#foodlist').replaceWith(el)
  }
  
  // key t: redraw with table view
  document.addEventListener('keyup', e => {
    if (e.key === 't')
      redraw(plats2table(plats))
  })