/* 
                                        Pratique de la POO

Créer une fonction permettant de construire des objets représentant un aliment
let fondue = new Food("fondue savoyarde", 516, 1365, 600, 500, 390, 450 )

// should be something like …
{
  "name": "fondue savoyarde",
  "weight": 516,
  "caloriesTotal": 1365,
  "calories": 600,
  "lipides": 500,
  "glucides": 390,
  "proteines": 450
}

Modifiez le constructeur Food …
Pour que les objets aient une propriété «calories» qui soit calculé à partir des nutriments de l’objet.
*/


function Food(name,weight,lipides,glucides,proteines){
    this.name = name
    this.weight = weight
    this.lipides = lipides
    this.glucides = glucides
    this.proteines = proteines
    this.calories = () => this.proteines * 4 + this.glucides * 4 + this.lipides * 9
    this.caloriesTotal = () => (this.calories * weight)/100
}

let magret = new Food("magret", 516, 25, 4.6, 15)
magret.calories = magret.calories()
magret.caloriesTotal = magret.caloriesTotal()

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Si le user nous passe un objet au lieu de weight, lipides, glucides etc.....

function Food(nameOrObject,weight,lipides,glucides,proteines){
    if (nameOrObject instanceof Object){
        Object.assign(this, nameOrObject)
    }
    else{
        this.name = nameOrObject
        this.weight = weight
        this.lipides = lipides
        this.glucides = glucides
        this.proteines = proteines
        this.calories = () => this.proteines * 4 + this.glucides * 4 + this.lipides * 9
        this.caloriesTotal = () => (this.calories * weight)/100
    }
}

let magret = new Food("magret", 516, 25, 4.6, 15)
magret.calories = magret.calories()
magret.caloriesTotal = magret.caloriesTotal()

// on définit un objet
let test = {
    "name": "fondue savoyarde",
    "weight": 516,
    "caloriesTotal": 1365,
    "calories": 600,
    "lipides": 500,
    "glucides": 390,
    "proteines": 450
  }
// on teste en créant un aliment en lui passant uniquement un objet
  new Food(test)

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* On remarque que this.calories & this.caloriesTotal sont des attributs partagés par chaque aliment,
on peut donc l'ajouter directement au prototype, c'est comme si on l'ajoutait directement 
dans la classe Food, alors que les autres attributs sont propres à l'objet, "à l'instance de classe" */

function Food(name,weight,lipides,glucides,proteines){
    this.name = name
    this.weight = weight
    this.lipides = lipides
    this.glucides = glucides
    this.proteines = proteines
}

/* ici on ne fait pas 
Food.prototype.calories = () => this.proteines * 4 + this.glucides * 4 + this.lipides * 9
car les this contenus dans les fat arrow se réfèrent au {} précédentes pour définir le this.
Or, ici il n'y en a pas donc le this ferait référence à window, c'est pourquoi nous allons choisir
la déclaration de function classique, avec les {} qui premettra au this de s'appliquer sur l'objet voulu  */

Food.prototype.calories = function(){ 
    return this.proteines * 4 + this.glucides * 4 + this.lipides * 9
}

Food.prototype.caloriesTotal = function(){ 
    return (this.calories * weight)/100
}
// on crée un objet:
let magret = new Food("magret", 516, 25, 4.6, 15)
/*
On l'affiche sur la console en faisant : magret

Voilà ce que cela affiche, on voit que les deux fonctions calories() & caloriesTotal() sont bien 
contenus dans la "classe supérieure" de l'objet crée (ils sont tjrs accessibles).

Food {name: "magret", weight: 516, lipides: 25, glucides: 4.6, proteines: 15}
glucides: 4.6
lipides: 25
name: "magret"
proteines: 15
weight: 516
    __proto__:
    calories: ƒ ()
    caloriesTotal: ƒ ()
    constructor: ƒ Food(name,weight,lipides,glucides,proteines)
    __proto__: Object
*/