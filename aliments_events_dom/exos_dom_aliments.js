// Décommenter la partie js que l'on veut tester, fichier relié au html, si tout décoché, 
// ça plante car variables doublement déclarées

///////////////////// Filter, Find et Map sont des méthodes qu'on applique sur des Tableaux ///////////////////// 
//////////////////// C'est pour cela qu'on transforme les éléments du DOM trouvés en tableaux ////////////////////

 
 /* 
 La méthode filter() crée et retourne un nouveau tableau contenant tous les éléments du tableau d'origine 
 qui remplissent une condition déterminée par la fonction callback.

 La méthode find() renvoie la valeur du premier élément trouvé dans le tableau qui respecte la condition 
 donnée par la fonction de test passée en argument. Sinon, la valeur undefined est renvoyée.

 La méthode map() crée un nouveau tableau avec les résultats de l'appel d'une fonction fournie sur 
 chaque élément du tableau appelant.


 SUJET DE L'EXERCICE
                                                    
Trouvez tous les aliments pauvres en glucides
Par exemple avec un taux de glucides < 10g pour 100g.
Suivez le plan suivant:

commencez par extraire du document la liste de tous les plats.  
les tags html correspondant sont des li avec ~class=”plat”~
ensuite, il va falloir filtrer cette première liste: imaginer comment faire.*/

 
/////////////////// Première correction en passant par les index  ///////////////////


 // on récupère tous les éléments plats et on transforme le tout en array pour pouvoir ensuite filtrer les plats dont les glucides sont < 10
 let plats = Array.from(document.querySelectorAll('.plat'))  

 // on va tester d'extraire le nb de glucides d'un seul plat pr comprendre comment faire
 let plat = plats[5]
 plat.querySelectorAll('dt') // on récupère tous les "dt" d'un plat

 plat.querySelectorAll('dt')[2].nextElementSibling // en partant du principe que tout le monde a bien mis le dt glucides en [2]
 plat.querySelectorAll('dt')[2].nextElementSibling.innerHTML // renvoie le nb de glucides "45", mais c'est une chaîne de caractères...
 parseFloat(plat.querySelectorAll('dt')[2].nextElementSibling.innerHTML) // on le transforme en entier = > on a récupéré le nb de glucides


 const glucides = (plat) =>  parseFloat(plat.querySelectorAll('dt')[2].nextElementSibling.innerHTML) // fonction qui renvoie le nb de glucide d'un plat

 // La méthode filter() crée et retourne un nouveau tableau contenant tous les éléments du tableau d'origine qui remplissent la condition (renvoie true)
 //  donc pour chaque plat, si les glucides sont < 10 alors le nouvel élément est envoyé ds un nouveau tableau
 plats.filter(plat => glucides(plat) < 10)

 // ça nous renvoie un tableau de 9 éléments, on veut créer un tableau qui affiche les glucides renvoyés
 plats.filter(plat => glucides(plat) < 10).map(plat => glucides(plat))

// fonction qui récupère le nom du plat
 const plat_name = (plat) => plat.querySelector('p').innerHTML

//  // ça nous renvoie un tableau de 9 éléments qui affiche les noms des plats dont le nb de glucides est < à 10
 plats.filter(plat => glucides(plat) < 10).map(plat_name)


 /////////////////// Seconde correction plus précise sans passer par les index  ///////////////////

let plats = Array.from(document.querySelectorAll('.plat'))

const glucide = plat => parseFloat(
  Array.from(plat.querySelectorAll('dt'))  // find all dt's in plat
    .find(el => el.innerHTML.toLowerCase() === 'glucides') // keep the one with glucides
    .nextElementSibling // get to the dd
    .innerHTML)

let ketodishes = plats.filter(plat => glucide(plat) < 10)

//                                              SUJET DE L'EXERCICE 2

//Colorez le nom des nutriments des plats lorsqu'on clique sur le titre du plat

let ul = document.querySelector("ul").addEventListener("click", function(e){
    if (e.target.tagName === "P" )
      c = e.target.closest("li").querySelector("dl").classList.toggle("colored")      
})


//                                              SUJET DE L'EXERCICE 3


/*
Web scraping
Créez à partir de la page une représentation javascript des données.
Par exemple:
const aliments = [ 
  { name: 'magret aux cèpes', 
    weight: 516, 
    caloriesTotal: 1365, 
    calories: 309, lipides: 25, 
    glucides: 4.6, protides: 15 }
  ]
*/

let plats = Array.from(document.querySelectorAll('.plat'))

const glucide = plat => parseFloat(
  Array.from(plat.querySelectorAll('dt'))  // find all dt's in plat
    .find(el => el.innerHTML.toLowerCase() === 'glucides') // keep the one with glucides
    .nextElementSibling // get to the dd
    .innerHTML)

const recherche_nutri = (plat, nutri) => parseFloat(
  Array.from(plat.querySelectorAll('dt'))  // find all dt's in plat
    .find(el => el.innerHTML.toLowerCase() === nutri) // keep the one with the wanted nutri
    .nextElementSibling // get to the dd
    .innerHTML)

const lipides = plat => recherche_nutri(plat, 'lipides')
const calories = plat => recherche_nutri(plat, 'calories')
const proteines = plat => recherche_nutri(plat, 'protéines')

const plat2js = plat => (
  {
    name : plat.querySelector('p').innerHTML,
    weight : parseFloat(plat.attributes['data-weight'].value),
    caloriesTotal : parseFloat(plat.attributes['data-calories'].value),
    calories : calories(plat),
    lipides : lipides(plat),
    glucides : glucide(plat),
    protides : proteines(plat)
  })

plats.map(plat2js) // je passe à plats.map une valeur fonction, que je vais appliquer à chaque élément de plat
const aliments = plats.map(plat2js)
console.log(aliments)


//                                              SUJET DE L'EXERCICE 4

// Rajoutez dans la page la liste des aliments, mais cette fois sous la forme d’un tableau html qui apparaîtra tout en bas de page.
const plat2jstr = p =>
 `<tr>
    <td>${p.name}</td><td>${p.calories}</td><td>${p.glucides}</td>
  </tr>`

const table = document.createElement('table')
// je prends la liste de tous mes plats(du tableau), 
// je prend un plat, je le trsfrme en objet JS et je le retransforme en tableau à afficher)
table.innerHTML = plats.map(p => plat2jstr(plat2js(p))).join(`\n`) 
document.body.appendChild(table)


